#!/bin/bash

function test() {
    local http_proxy=http://localhost:12080

    local test_forward_result=false
    local test_deb_httpredir_result=false
    local test_deb_security_result=false
    local test_deb_ppa_result=false
    local test_gem_result=false

    local temp_dir=$(mktemp -d)
    echo "Results will be written inside the temporary directory '$temp_dir'"

    echo "Testing forward proxy"
    wget -P $temp_dir -e use_proxy=yes -e http_proxy=$http_proxy "http://www.google.fr"
    if grep "J'ai de la chance" $temp_dir/index.html ; then test_forward_result=true; fi

    echo "Testing DEB httpredir"
    wget -P $temp_dir --header="Host: httpredir.debian.org" $http_proxy/debian/pool/main/libd/libdrm/libdrm-intel1_2.4.58-2_amd64.deb
    if dpkg -c $temp_dir/libdrm-intel1_2.4.58-2_amd64.deb | grep libdrm_intel.so ; then test_deb_httpredir_result=true; fi

    echo "Testing DEB security"
    wget -P $temp_dir --header="Host: security.debian.org" $http_proxy/dists/jessie/updates/InRelease
    if grep "Label: Debian-Security" $temp_dir/InRelease ; then test_deb_security_result=true; fi

    echo "Testing DEB ppa"
    wget -P $temp_dir --header="Host: ppa.launchpad.net" $http_proxy/webupd8team/java/ubuntu/dists/trusty/Release
    if grep "Origin: LP-PPA-webupd8team-java" $temp_dir/Release ; then test_deb_ppa_result=true; fi

    echo "Testing GEM"
    # TODO

    echo "Deleting the temporary directory '$temp_dir'"
    # rm -rf $temp_dir

    echo
    echo "forward proxy ... $test_forward_result"
    echo "DEB httpredir ... $test_deb_httpredir_result"
    echo "DEB security .... $test_deb_security_result"
    echo "DEB ppa ......... $test_deb_ppa_result"
    echo "GEM ............. TODO $test_gem_result"
}

test $@
